import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

var elements = Array.from(document.getElementsByClassName('eretailer-wrapper-app'));

for(var el in elements){
  var atts = JSON.parse( elements[el].dataset.atts );

  new Vue({
      el: elements[el],
      components: { App },
      render(h) {
        return h(App, { props: {'atts': atts } });
      }
  });
}
