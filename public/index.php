<?php

$cssFile = 'bf-epremise-button.css';
if ( isset($_GET['locale']) && !empty($_GET['locale']) ) {
    if ( file_exists('./css/bf-epremise-button-'.$_GET['locale'].'.css') ) {
        $cssFile = 'bf-epremise-button-'.$_GET['locale'].'.css';
    }
}

?>

<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title>ePremise Front End Examples</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="./css/<?php echo $cssFile; ?>?v=1.0.1">
</head>

<body>
    <h1>ePremise Locator Front-End Examples</h1>
    <h2>Button with Dialog</h2>
    <div id='eretailer-wrapper' class='eretailer-wrapper-app' data-atts='{
        "market_product_code":"Australia_000000000050234035",
        "button_label":"Buy Now",
        "button_image":null,
        "banner_image":null,
        "fullscreen":false,
        "legalcopy":"By choosing one of links above, you are leaving [Domain] and going to a different website. All content on the retailer web site is the responsibility of its owner including, without limitation, protection of your privacy which is governed by the policy of that web site. We encourage you to review the terms of use, privacy and cookie policies of that website.",
        "noresults":"No results for your location.",
        "introtext":"Bring Home a Bottle Of Old No. 7"
    }'><div>loading eRetailer data...</div></div>

    <br>
    <h2>Button with Banner</h2>
    <div id='eretailer-wrapper' class='eretailer-wrapper-app' data-atts='{
        "market_product_code":"United_States_1000000000000009963000",
        "button_label":"Buy Now",
        "button_image":null,
        "banner_image": "https://ecommerce-static.brown-forman.com/Banner_images/0000_jdtw.jpg",
        "fullscreen":false,
        "legalcopy":"By choosing one of links above, you are leaving [Domain] and going to a different website. All content on the retailer web site is the responsibility of its owner including, without limitation, protection of your privacy which is governed by the policy of that web site. We encourage you to review the terms of use, privacy and cookie policies of that website.",
        "noresults":"No results for your location.",
        "introtext":"Bring Home a Bottle Of Old No. 7"
    }'><div>loading eRetailer data...</div></div>

    <br>
    <h2>Button with Full Screen Pop-Over</h2>
    <div id='eretailer-wrapper' class='eretailer-wrapper-app' data-atts='{
        "market_product_code":"United_States_1000000000000009963000",
        "button_label":"Buy Now",
        "button_image":null,
        "banner_image":null,
        "fullscreen":true,
        "legalcopy":"By choosing one of links above, you are leaving [Domain] and going to a different website. All content on the retailer web site is the responsibility of its owner including, without limitation, protection of your privacy which is governed by the policy of that web site. We encourage you to review the terms of use, privacy and cookie policies of that website.",
        "noresults":"No results for your location.",
        "introtext":"Bring Home a Bottle Of Old No. 7"
    }'><div>loading eRetailer data...</div></div>

    <br>
    <h2>Image Button</h2>
    <div id='eretailer-wrapper' class='eretailer-wrapper-app' data-atts='{
        "market_product_code":"United_States_1000000000000009963000",
        "button_label":"Buy Now",
        "button_image":"./img/jackdanielsbottle.png",
        "banner_image":null,
        "fullscreen":false,
        "legalcopy":"By choosing one of links above, you are leaving [Domain] and going to a different website. All content on the retailer web site is the responsibility of its owner including, without limitation, protection of your privacy which is governed by the policy of that web site. We encourage you to review the terms of use, privacy and cookie policies of that website.",
        "noresults":"No results for your location.",
        "introtext":"Bring Home a Bottle Of Old No. 7"
    }'><div>loading eRetailer data...</div></div>

    <script type="text/javascript" src="./js/bf-epremise-button.js?v=1.0.11"></script>
</body>

</html>
