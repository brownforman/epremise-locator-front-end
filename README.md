

### Attributes:

Attributes are passed to any div containing the class "eretailer-wrapper-app". They should be passed as a stringified-JSON object to the data-atts attribute of the div.

See the examples in public/index.html.

+ market_product_code : String (Salesforce Product Code)
+ button_label : String (free text)
+ fullscreen : Boolean (true | false, optional)
+ banner_image : String - (URL, optional)
+ button_image : String - (URL, optional) - Replace standard button with a clickable image
+ introtext : Copy to appear at the top of the dialog/pop-over
+ noresults : Copy to appear if a subgeography has no results
+ legalcopy : Copy to appear at bottom of dialog/pop-over

### Find Market Product Codes:
https://epremise-api.b-fonline.com/tools/market_product_codes

### For Developers

The plugin should be updated to match the look and feel of the website it is on. This is accomplished by making edits to the files in resources/sass/ and the files in resources/js.

To compile edits, follow these steps:
1. Run `npm install`
2. Run `npm run watch` to monitor the resources directory for changes and compile automatically.
3. Run `npm run prod` to compile changes for production.

`npm run dev` can be used to compile a non-minified version of the code.

**Country paramenter build**

Make a copy of the `sass/LocaleCodeTemplate` with the exact name from the [Wordpress Locale Code](https://wpcentral.io/internationalization/). Make all stylesheet customizations under the newly created folder (e.g. en_US). Build the stylesheet using the commands below.

`npm run dev --locale=en_US`

`npm run production --locale=en_US`


The build system uses Laravel Mix, Vue & Webpack. To keep build system updated, periodically check changes here and apply any that are necessary to keep the build system working: https://github.com/laravel/laravel/blob/master/package.json and https://laravel.com/docs/5.8/frontend#using-react.

**Multiple custom stylesheet**

Use public/index.php to access different custom stylesheets.

`public/index.php?locale=en_US` OR `public/?locale=en_US`

You can export the compiled code from the public folder to wherever you need it in your site structure.

Documentation of the Brown-Forman Api can be found at https://epremise-api.b-fonline.com.
```
